'''
运行本文件及  OORD.exe  之后，运行：addnewline4BL.py
本文件从cad中获取实线、虚线、路径...等实体类别的XY坐标（高斯坐标），并写入文件。
需要输入存放数据的文件夹的名字
'''
from pyautocad import Autocad, APoint
import easygui as g
import os
acad = Autocad(create_if_not_exists=True)
currentworkdir=os.getcwd()
reply = g.enterbox(title=' 新建文件夹:', msg='请输入存放数据的文件夹的名字：')
if reply:
	if not os.path.exists(reply):
		os.makedirs(reply)
	title = "请选择新建文件的名字："
	msg ="请选择："
	choices=['道路','实线','虚线','实线和虚线','停止线','路径','红绿灯','正方形区域']
	choice = g.choicebox(msg, title, choices)
	if choice:
		filename = g.msgbox("你选择了: " + str(choice), "选择结果：")
		for item in acad.iter_objects("PolyLine"):
			char=item.Coordinates
			char_1=list(char)
			filefullpath=currentworkdir+'\\'+reply+'\\'+choice+'.txt'
			with open(filefullpath,'w+',encoding='utf-8') as f:
				for i in range(int(len(char_1) / 2)):
					char_2 = [str(round(char_1[2 * i + 1], 5)), str(round(char_1[i * 2], 5))]
					f.writelines(','.join(char_2))
					f.writelines('\n')
	title_1 = "请选择新建文件的名字(节点个数文件)："
	choicesofnodes = ['每条道路节点个数','每条实线节点个数', '每条虚线节点个数','实线和虚线节点个数', '每条停止线节点个数','每个路径节点个数','红绿灯节点个数','正方形区域节点个数']
	choice_1 = g.choicebox(msg, title_1, choicesofnodes)
	if choice_1:
		filename = g.msgbox("你选择了: " + str(choice_1), "选择结果：")
		nodeamounts = []
		for item in acad.iter_objects("PolyLine"):
			char = item.Coordinates
			char_1 = list(char)
			nodeamounts.append(int(len(char_1) / 2))
			filefullpath = currentworkdir +'\\'+reply+ '\\' + choice_1 + '.txt'
		with open(filefullpath, 'a+', encoding='utf-8') as f:  #以追加的方式从cad高斯高斯坐标
			for i in nodeamounts:
				f.writelines(str(i))
				f.writelines('\n')





