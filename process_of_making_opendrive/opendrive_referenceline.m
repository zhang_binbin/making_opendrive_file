%本程序求道路参考线的参数三次多项式的参数，形如：
%{
u（第1列） v（第2列）表达式的参数：
[   252.45751483578203533397754654288, -0.5265130944237235155469534220174]
[ 0.027336694800787861292512559430179,   9.890039281779490210055882926099]
[ -0.32025011053270074912191489602264,  1.0504486814855831244841510851984]
%}
clc;
clear;
close all;
pointCloudData = readpointcloudfile2('D:\qcc\HDmap_statistics_opendrive\python_code\123\实线.txt');
xh=pointCloudData(:,1);
yh=pointCloudData(:,2);

%xh=xh(end:-1:1);%数据点反序，假如坐标形式是：3465440.8317,557125.2222,6.1575则不用反序，
%yh=yh(end:-1:1);%这个目的是保证南北方向和实际方向一致
figure(1);hold on;plot(yh,xh,'r.');axis equal;axis off;
figure(1);hold on;plot(yh,xh,'b');axis equal;axis off;
figure(1);hold on;title('原始数据');
[b1 s1 ]=polyfit(xh,yh,3); %s1中包含拟合后的残差的二范信息
std_Gauss=sqrt((s1.normr)^2/s1.df);
move=[xh(1) yh(1)];

azimuth_angle=[];
hdg_a=[];
for i=2:3
    [angle,hdg_t]=calculate_azimuth(xh(i-1),yh(i-1),xh(i),yh(i));
    azimuth_angle=[azimuth_angle;angle];
    hdg_a=[hdg_a,hdg_t];
end
hdg=mean(hdg_a);
alfa=mean(azimuth_angle);
degree=rad2deg(alfa);
rotate=[cos(alfa)  -sin(alfa) ;
       sin(alfa)  cos(alfa)];
re=[xh-xh(1) yh-yh(1)]*rotate;%先平移后旋转 ，把高斯平面坐标转化到u v坐标系中的坐标，把高斯坐标中的X坐标轴旋转到和U坐标轴一致，u坐标轴的方向大致和道路走向一致，是道路开始处的切向，
newpoint=re;
figure(2);hold on;plot(newpoint(:,2),newpoint(:,1),'r.');axis equal;axis off;
figure(2);hold on;title('点在u-v坐标系中的坐标');
xh_1=newpoint(:,1);
yh_1=newpoint(:,2);
%求坐标点在track坐标系中组成路线的总长度，暂时考虑的是投影到平面上的长度，也许需要考虑道路在三维空间中的长度
total_len=0;
p=[];%离散点处累计的路线长度和总的路线长度的比值，从p（2）开始
p(1)=0;
for i=2:size(xh_1,1)
    len=sqrt((xh_1(i)-xh_1(i-1))^2+(yh_1(i)-yh_1(i-1))^2);
    total_len=total_len+len;
end
total=0;
for i=2:size(xh_1,1)
    haha=sqrt((xh_1(i)-xh_1(i-1))^2+(yh_1(i)-yh_1(i-1))^2);
    total=total+haha;
    p=[p total/total_len];
end
%{
方程组的形式：
u=bu*p+cu*p^2+du*p^3
v=     cv*p^2+dv*p^3
共5个参数，选择3个点可以列出6个参数方程，从而解出参数
%}

med=ceil(size(p,2)/2);%median ,中间值（的）
P=[p(2) p(2)^2 p(2)^3;
    p(med) p(med)^2 p(med)^3;
    p(end) p(end)^2 p(end)^3];%系数矩阵  用三个点去拟合参数三次多项式的6个参数

y=[newpoint(2,1),newpoint(2,2);
    newpoint(med,1),newpoint(med,2);
    newpoint(end,1),newpoint(end,2)];%方程组中的u  v坐标

parameter=P\y;
%根据拟合的参数计算u v坐标系中的坐标
sum=0;
for i=1:size(p,2)
    ac=[p(i) p(i)^2 p(i)^3]*parameter;
    sum=sum+(ac(1)-newpoint(i,1))^2;%u坐标相减的平方和
end 

std_1=sqrt(sum/(size(p,2)-1));


[b s ]=polyfit(xh_1,yh_1,3); %s中包含拟合后的残差的二范信息
std=sqrt((s.normr)^2/s.df);
disp(['高斯坐标系下所有点进行三次多项式拟合的标准差:',num2str(std_Gauss)]);
disp(['s t坐标系下所有点进行三次多项式拟合的标准差:',num2str(std)]);
disp(['s t坐标系下3个点进行三次多项式拟合的标准差:',num2str(std_1)]);
disp('hdg用弧度表示：');
disp(vpa(hdg));
disp('hdg用度数表示：');
hdg_m=rad2deg(hdg);
disp(vpa(hdg_m));
disp('路线总长度：');
disp(vpa(total_len));
disp('u v表达式的参数：');
disp(vpa(parameter));
%Pb=polyval(b,xh_1);
%figure(3);hold on;plot(xh_1,yh_1,'b.',xh_1,Pb,'r-');axis equal;




function [azimuth,hdg]=calculate_azimuth(xa,ya,xb,yb)
%-----------------------------------------%
%这个函数的目的是在高斯平面直角坐标系中根据象限角计算方位角
%暂时没考虑deltax=0或者deltay=0的情况。
%参考：https://www.cnblogs.com/yibeimingyue/p/12611697.html
%Author：Changcai QIN
%Date:2020/4/1
%-----------------------------------------%
angle=atan((yb-ya)/(xb-xa));%计算象限角
angle=abs(angle);
deltax_AB=xb-xa;
deltay_AB=yb-ya;
    if deltax_AB>0 && deltay_AB>0 %方向在第1象限
        azimuth=angle;
        hdg=pi/2-angle;%hdg是道路参考线（reference line）的航向 ，是x轴（横轴）正向逆时针旋转到refernece line的方向的角度
    elseif deltax_AB<0 && deltay_AB<0 %方向在第3象限
        azimuth=angle+pi;
        hdg=pi*3/2-angle;
    elseif deltax_AB>0 && deltay_AB<0 %方向在第4象限
        azimuth=pi*2-angle;
        hdg=pi/2+angle;
    elseif deltax_AB<0 && deltay_AB>0 %方向在第2象限
        azimuth=pi-angle; 
        hdg=pi*3/2+angle;
    end 
end





