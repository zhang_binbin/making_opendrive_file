%本程序做道路纵断面内沿着参考线的高程拟合
clc;clear;
close all;
pointCloudData = readpointcloudfile2('D:\qcc\HDmap_statistics_opendrive\python_code\123\实线.txt');
x=pointCloudData(:,1);
y=pointCloudData(:,2);
z=pointCloudData(:,3);
figure(100);hold on;plot(y,x,'r.');axis equal;axis off;
figure(100);hold on;plot(y,x,'b');axis equal;axis off;
figure(100);hold on;title('原始数据');
std=[];%存放直线拟合的标准差
len=0;
len_s=[];
len_s(1)=0;
section_parameter=[];%存放每一小段拟合的三次多项式的4个参数a b c d
for i=4:3:size(x,1)
    tempx=x(i-3:i,1);
    tempy=y(i-3:i,1);
    tempz=z(i-3:i,1);
    [b ,s]=polyfit(tempx,tempy,1); %用四个点做直线拟合，s.normr是残差平方和
    for j=2:size(tempx,1)
        len=len+sqrt((tempx(j,1)-tempx(j-1,1)).^2+(tempy(j,1)-tempy(j-1,1)).^2);%这个就是需要的len
    end
    std=[std ;sqrt((s.normr)^2/s.df)];%直线拟合的标准差
    po=polyval(b,tempx);%查询拟合后的y（po）值
    %sqrt(sum((tempy-po).^2)/2)
    figure(1);hold on;plot(y,x,'r.');axis equal;%原始数据点
    figure(1);hold on;plot(po,tempx,'-','color',[rand rand rand]);axis equal;axis off;%画出拟合的线

   
    %[~,ind]=min(tempy);
    original=[tempx(1,1) tempy(1,1)];%因为车道线方向总是和行驶方向相同，4个点中的第1个点作为投影原点
    %figure(1);hold on;plot(original(1),original(2),'go');
    dist=sqrt((tempx-original(1)).^2+(tempy-original(2)).^2);%4个点到第一个点的距离
    dist_1=abs(b(1).*tempx+b(2)-tempy)./sqrt(1+b(1)^2);%4个点到拟合直线的垂直距离，相当于竖直断面的横坐标,暂时没用到
    %dist_2=sqrt(dist.^2-dist_1.^2);%竖直断面的纵坐标
    section=[dist tempz];%三维变成二维,这个是高程断面上位置和高程表示的坐标对，在不同的位置上高程不同，各点离第一个点的长度是自变量，高程是函数值
    [b_section ,s]=polyfit(section(:,1),section(:,2),3); %用三次多项式做高程拟合，b_section就是需要的系数，s.normr是残差平方和
    section_parameter=[section_parameter; b_section];
    len_s=[len_s;len];%这个是需要的s,最后一个值用不到
end

if size(x,1)-i==1 %剩余1点的情况 
    tempx=x(end-3:end,1);
    tempy=y(end-3:end,1);
    [b ,s]=polyfit(tempx,tempy,1); %虽然剩余一个点，但是用最后4个点做直线拟合，s.normr是残差平方和的平方根
    
    %len=0;
    for j=4:size(tempx,1)
        len=len+sqrt((tempx(j,1)-tempx(j-1,1)).^2+(tempy(j,1)-tempy(j-1,1)).^2);%这个就是需要的len
    end
    
    std=[std ;sqrt((s.normr)^2/s.df)];%最后4个点直线拟合的标准差
    po=polyval(b,tempx(:,1));%查询拟合后的y（po）值
    figure(1);hold on;plot(tempx(:,1),po,'-','color',[rand rand rand]);axis equal;%画出拟合的线
    
    %[~,ind]=min(tempy);
    original=[tempx(1,1) tempy(1,1)];%因为车道线方向总是和行驶方向相同，4个点中的第1个点作为投影原点
    %figure(1);hold on;plot(original(1),original(2),'go');
    dist=sqrt((tempx-original(1)).^2+(tempy-original(2)).^2);%4个点到第一个点的距离
    dist_1=abs(b(1).*tempx+b(2)-tempy)./sqrt(1+b(1)^2);%4个点到拟合直线的垂直距离，相当于竖直断面的横坐标
    %dist_2=sqrt(dist.^2-dist_1.^2);%竖直断面的纵坐标
    section=[dist tempz];%三维变成二维
    [b_section ,s]=polyfit(section(:,1),section(:,2),3); %用三次多项式做高程拟合，b_section就是需要的系数，s.normr是残差平方和
    section_parameter=[section_parameter ;b_section];
    len_s=[len_s;len];%这个是需要的s  最后一个值用不到
    
    
    
    
elseif size(x,1)-i==2 %剩余2点的情况 
    tempx=x(end-3:end,1);
    tempy=y(end-3:end,1);
    [b ,s]=polyfit(tempx,tempy,1); %虽然剩余2个点，但是用最后4个点做直线拟合，s.normr是残差平方和
    
    %len=0;
    for j=3:size(tempx,1)
        len=len+sqrt((tempx(j,1)-tempx(j-1,1)).^2+(tempy(j,1)-tempy(j-1,1)).^2);%这个就是需要的len
    end
    
    std=[std ;sqrt((s.normr)^2/s.df)];%直线拟合的标准差
    po=polyval(b,tempx(:,1));%查询拟合后的y（po）值
    figure(1);hold on;plot(po,tempx(:,1),'-','color',[rand rand rand]);axis equal;%拟合的线
    
    %[~,ind]=min(tempy);
    original=[tempx(1,1) tempy(1,1)];%因为车道线方向总是和行驶方向相同，4个点中的第1个点作为投影原点
    %figure(1);hold on;plot(original(1),original(2),'go');
    dist=sqrt((tempx-original(1)).^2+(tempy-original(2)).^2);%4个点到第一个点的距离
    dist_1=abs(b(1).*tempx+b(2)-tempy)./sqrt(1+b(1)^2);%4个点到拟合直线的垂直距离，相当于竖直断面的横坐标
    %dist_2=sqrt(dist.^2-dist_1.^2);%竖直断面的纵坐标
    section=[dist tempz];%三维变成二维
    [b_section ,s]=polyfit(section(:,1),section(:,2),3); %用三次多项式做高程拟合，b_section就是需要的系数，s.normr是残差平方和
    section_parameter=[section_parameter ;b_section];
    len_s=[len_s;len];%这个是需要的s  最后一个值用不到
end


%以下写文件，形式如：
%{
			<elevation s="0" a="157.94832782282703" b="0.0045902037095718103" c="-9.7911202617815814e-05" d="3.8146393268851109e-08" />
			<elevation s="10.054508483611135" a="157.98462068755995" b="0.0026328746909716171" c="-9.9793159582580547e-05" d="3.3976100465736836e-07" />
			<elevation s="20.109016967222271" a="158.00134989184087" b="0.00072917487300723114" c="-0.00015959005050915516" d="6.7030862138480063e-06" />
			<elevation s="30.163525450833404" a="157.99936122463387" b="-0.0004471160443026978" c="-3.986217910051356e-05" d="1.5745861166329443e-06" />
%}
fd=fopen('D:\qcc\HDmap_statistics_opendrive\python_code\123\1113.txt','w+'); %写文件
for i=2:size(len_s,1)
	fprintf(fd,'      <elevation s="%f" a="%.20f" b="%.20f" c="%.20f" d="%.20f" />\r',[len_s(i-1) section_parameter(i-1,4) section_parameter(i-1,3) section_parameter(i-1,2) section_parameter(i-1,1)]);
end
fclose(fd);















